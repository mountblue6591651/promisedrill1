const p1 = new Promise((resolve, reject) => {
  try {
    setTimeout(() => {
      resolve("Promise 1 resolved");
    }, 1500);
  } catch (error) {
    reject(error);
  }
});

const p2 = new Promise((resolve, reject) => {
  try {
    setTimeout(() => {
      resolve("Promise 2 resolved");
    }, 2500);
  } catch (error) {
    reject(error);
  }
});

const p3 = new Promise((resolve, reject) => {
  try {
    setTimeout(() => {
      resolve("Promise 3 resolved");
    }, 3500);
  } catch (error) {
    reject(error);
  }
});

function composePromises(...promises) {
  const finalPromise = new Promise((resolve, reject) => {

    Promise.allSettled(promises)
    .then((results) => {
        resolve(results);
    }
    )
    .catch((error) => {
        reject(error);
    })
  });

  return finalPromise;
}

let finalResult = composePromises(p1, p2, p3)
                    .then((messages) => {
                        console.log(messages);
                    })
                    .catch((error) => {
                        console.log(error);
                    });

console.log(finalResult);
