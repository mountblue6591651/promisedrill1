function dynamicChain(...promiseFunctions) {

    let resolved = Promise.resolve();

    for(let index = 0;index<promiseFunctions.length;index++){
        resolved = resolved.then(promiseFunctions[index]);
    }

    return resolved;
}

function p1() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Promise 1 resolved");
        }, 1500);
    });
}

function p2() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Promise 2 resolved");
        }, 1500);
    });
}

function p3() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Promise 3 resolved");
        }, 1500);
    });
}

dynamicChain(p1, p2, p3)
    .then((result) => {
        console.log(result);
    })
    .catch((error) => {
        console.error(error);
    });
