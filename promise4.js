function parallelLimit(promiseArray, limit) {
  // Initialize variables
  let index = 0;
  let limitedArray = promiseArray.slice(index, index + limit);
  let resolvedArr = [];

  // Create a new Promise to handle asynchronous operations
  const result = new Promise((resolve, reject) => {
      // Recursive function to handle resolving promises in batches
      function resolver() {
          // Use Promise.allSettled to wait for all promises in the batch to settle
          Promise.allSettled(limitedArray).then((result) => {
              // Concatenate the results of the current batch to the overall resolved array
              resolvedArr = [...resolvedArr, ...result];
              index += limit;

              // Check if there are more promises to process
              if (index < promiseArray.length) {
                  // Call the resolver function recursively for the next batch
                  resolver();
              } else {
                  // Resolve the main promise with the final result array
                  resolve(resolvedArr);
              }

          })
          .catch((error) => {
              // Reject the main promise if there's an error in the process
              reject(error);
          });
      }

      // Start the recursive resolution process
      resolver();
  });

  // Return the main promise
  return result;
}

// Create example promises
const p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve("Promise 1 resolved");
  }, 1500);
});

const p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve("Promise 2 resolved");
  }, 2500);
});

const p3 = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve("Promise 3 resolved");
  }, 3500);
});

const p4 = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve("Promise 4 resolved");
  }, 4500);
});

// Create an array of promises
const promiseArray = [p1, p2, p3, p4];

// Set the limit for parallel execution
let limit = 2;

// Call the parallelLimit function with the array of promises and the limit
parallelLimit(promiseArray, limit).then((res) => {
  // Log the final result
  console.log(res);
})
.catch((err) => {
  // Log any errors that occur during the process
  console.error(err);
});
