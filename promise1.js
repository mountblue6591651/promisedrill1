// Function to create a Promise that resolves after 1.5 seconds
function racePromise1() {
  const promise1 = new Promise((resolve, reject) => {
    try {
      setTimeout(() => {
        resolve("Promise 1 resolved");
      }, 1500);
    } catch (error) {
      reject(error);
    }
  });

  return promise1;
}

// Function to create a Promise that resolves after 2.5 seconds
function racePromise2() {
  const promise2 = new Promise((resolve, reject) => {
    try {
      setTimeout(() => {
        resolve("Promise 2 resolved");
      }, 2500);
    } catch (error) {
      reject(error);
    }
  });

  return promise2;
}

// Function to race the execution of the two promises
function racePromises() {
  // Use Promise.race to resolve with the result of the first resolving promise
  Promise.race([racePromise1(), racePromise2()])
    .then((message) => {
      console.log(message); // Log the resolved message
    })
    .catch((error) => {
      console.error(error); // Log any errors that occur
    });
}

// Call the function to initiate the racing of promises
racePromises();
